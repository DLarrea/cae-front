export const environment = {
    appName: 'CAE System',
    appVersion: 'v1.0.0',
    api: 'https://csa-caesystem.site/api',
    // api: 'http://localhost:8000',
    authGET: true
};
