import { MenuItem } from "primeng/api";

const itemHome : MenuItem = {
    icon: 'pi pi-home',
    routerLink: '/inicio'
}

const sexosOptions = [
    {
        label: 'Masculino',
        value: 'M'
    },
    {
        label: 'Femenino',
        value: 'F'
    }
]

export {
    itemHome,
    sexosOptions
}