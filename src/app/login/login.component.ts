import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment.development';
import { AuthService } from '../services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { Response } from '../utils/response';
import { MessageService } from 'primeng/api';
import { fieldValidator, formValidator } from '../utils/formValidator';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements AfterViewInit {

  @ViewChild('captcha') captchaElement!:ElementRef;
  @ViewChild('captcha2') captchaElement2!:ElementRef;
  captcha = `${environment.api}/scripts/captcha.php`;
  appName = environment.appName;
  form: FormGroup;
  formRC: FormGroup;
  loading: boolean = false;
  loadingRC: boolean = false;
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  showRecuperarCuenta: boolean = false;


  constructor(private authService: AuthService, 
    private formBuilder: FormBuilder,
    private router: Router,
    private toast: MessageService,
    private loader: NgxUiLoaderService){
    this.form = this.formBuilder.group({
      email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")]],
      pass: [null, [Validators.required]],
      captcha: [null, [Validators.required]],
    });
    this.formRC = this.formBuilder.group({
      documento: [null, [Validators.required]],
      email: [null, [Validators.required]],
      captcha: [null, [Validators.required]],
    });
  }

  ngAfterViewInit(): void {
    this.setCaptcha();
  }

  setCaptcha = () => {
    let img = <HTMLImageElement> this.captchaElement.nativeElement;
    this.captcha = this.captcha + '?update=' + new Date().getTime();
    img.src = this.captcha;
  }
  
  setCaptcha2 = () => {
    let img = <HTMLImageElement> this.captchaElement2.nativeElement;
    this.captcha = this.captcha + '?update=' + new Date().getTime();
    img.src = this.captcha;
  }

  login = () => {
    if(this.form.valid){
      this.loading = true;
      this.authService.login(this.form.value).subscribe({
        next: (response: Response) => {
          let token = response.result;
          localStorage.setItem('token', token);
          this.authService.setUser();
          this.router.navigate(['/inicio']);
          this.loading = false;
        },
        error: (err:HttpErrorResponse) => {
          let error : Response = err.error;
          this.toast.add({key: 'app', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          this.setCaptcha();
          this.form.reset();
          this.loading = false;
        }
      })
    }
  }
  
  recuperarCuenta = () => {
    if(this.formRC.valid){
      this.loader.start();
      this.authService.recuperarCuenta(this.formRC.value).subscribe({
        next: (response: Response) => {
          this.formRC.reset();
          this.setCaptcha();
          this.setCaptcha2();
          this.showRecuperarCuenta = false;
          this.toast.add({key: 'app', severity:'success', summary:'Éxito', detail: 'Se ha enviado una nueva contraseña a tu correo'});
          this.loader.stop();
        },
        error: (err:HttpErrorResponse) => {
          let error : Response = err.error;
          this.toast.add({key: 'app', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          this.setCaptcha();
          this.formRC.reset();
          this.loader.stop();
        }
      })
    }
  }

}
