import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class EstudiantesService {

  constructor(private http: HttpClient) { }

  get = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/estudiante`);
  }
  
  getAsistenciasEstudiantesById = (idCurso: number, idMateria: number, idUsuario: number):Observable<Response | any> => {
    let params = new HttpParams();
    params = params.append('curso', idCurso);
    params = params.append('materia', idMateria);
    params = params.append('usuario', idUsuario);
    return this.http.get<Response | any>(`${environment.api}/usuario-estudiantes`,{params});
  }

  getAsistenciasByEstudianteId = (id: number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/estudiante-asistencias/${id}`);
  }
  
  justificarAusenciaEstudiante = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.api}/estudiante-justificar-ausencia`, form);
  }
  
  getById = (id: number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/estudiante/${id}`);
  }

  post = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.api}/estudiante`, form);
  }

  put = (form:any):Observable<Response | any> => {
    return this.http.put<Response | any>(`${environment.api}/estudiante/${form.id}`, form);
  }

  delete = (id:number):Observable<Response | any> => {
    return this.http.delete<Response | any>(`${environment.api}/estudiante/${id}`);
  }

  registrarAsistencia = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.api}/registrar-asistencia`, form);
  }

}
