import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  constructor(private http: HttpClient) { }

  get = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/curso`);
  }
  
  getById = (id:number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/curso/${id}`);
  }
  getProfesorCursosMateriasById = (id:number,idCurso: number):Observable<Response | any> => {
    let params = new HttpParams();
    params = params.append('curso',idCurso);
    return this.http.get<Response | any>(`${environment.api}/usuario-materias/${id}`,{params});
  }
  getEstudiantesByCursoId = (id:number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/curso-estudiantes/${id}`);
  }
  
  getMateriaProfesorByCursoId = (id:number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/curso-materia-profesor/${id}`);
  }

  post = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.api}/curso`, form);
  }
  
  postAsociar = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.api}/curso-asociar`, form);
  }

  put = (form:any):Observable<Response | any> => {
    return this.http.put<Response | any>(`${environment.api}/curso/${form.id}`, form);
  }

  delete = (id:number):Observable<Response | any> => {
    return this.http.delete<Response | any>(`${environment.api}/curso/${id}`);
  }
  
  deleteCursoMateriaProfesor = (id:number):Observable<Response | any> => {
    return this.http.delete<Response | any>(`${environment.api}/curso-materia-profesor/${id}`);
  }
}
