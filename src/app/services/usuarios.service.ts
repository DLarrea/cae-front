import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private http: HttpClient) { }

  get = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/usuario`);
  }

  getCursosByUsuarioId= (id: number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/usuario-cursos/${id}`);
  }
  
  getEstudiantesACargo = (id: number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/responsable-estudiantes/${id}`);
  }

  getById = (id: number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/usuario/${id}`);
  }

  post = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.api}/usuario`, form);
  }

  put = (form:any):Observable<Response | any> => {
    return this.http.put<Response | any>(`${environment.api}/usuario/${form.id}`, form);
  }
  
  cambiarEstado = (form:any):Observable<Response | any> => {
    return this.http.put<Response | any>(`${environment.api}/usuario-estado/${form.id}`, form);
  }

  delete = (id:number):Observable<Response | any> => {
    return this.http.delete<Response | any>(`${environment.api}/usuario/${id}`);
  }

  getRoles = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/rol`);
  }
}
