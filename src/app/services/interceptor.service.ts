import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(private router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const token: string | null = localStorage.getItem('token');

    if (token) {
      req = req.clone({
        setHeaders: {
          Authorization : `Bearer ${token}`,
          rejectUnauthorized : 'false',
          Accept: 'application/json'
        }
      });
    }

    return next.handle(req).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      }),
      catchError((err: HttpErrorResponse) => {
        if (err.status == 401) {
          localStorage.removeItem('token');
          this.router.navigate(['/iniciar-sesion']);
        }
        if (err.status == 403) {
          this.router.navigate(['/inicio']);
        }
        return throwError(err);
      })
    );
  }
}
