import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class MateriasService {

  constructor(private http: HttpClient) { }

  get = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/materia`);
  }
  
  getHistorialAsistencias = (curso:number, materia:number, usuario: number):Observable<Response | any> => {
    let params = new HttpParams();
    params = params.append('curso', curso);
    params = params.append('materia', materia);
    params = params.append('usuario', usuario);
    return this.http.get<Response | any>(`${environment.api}/historial-asistencias`, {params});
  }

  post = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.api}/materia`, form);
  }

  put = (form:any):Observable<Response | any> => {
    return this.http.put<Response | any>(`${environment.api}/materia/${form.id}`, form);
  }

  delete = (id:number):Observable<Response | any> => {
    return this.http.delete<Response | any>(`${environment.api}/materia/${id}`);
  }
}
