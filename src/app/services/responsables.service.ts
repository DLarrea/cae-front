import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class ResponsablesService {

  constructor(private http: HttpClient) { }

  get = ():Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/responsable`);
  }
  
  getEstudiantesACargo = (id: number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/responsable-estudiantes/${id}`);
  }
  
  getById = (id: number):Observable<Response | any> => {
    return this.http.get<Response | any>(`${environment.api}/responsable/${id}`);
  }

  post = (form:any):Observable<Response | any> => {
    return this.http.post<Response | any>(`${environment.api}/responsable`, form);
  }

  put = (form:any):Observable<Response | any> => {
    return this.http.put<Response | any>(`${environment.api}/responsable/${form.id}`, form);
  }

  delete = (id:number):Observable<Response | any> => {
    return this.http.delete<Response | any>(`${environment.api}/responsable/${id}`);
  }
  
}
