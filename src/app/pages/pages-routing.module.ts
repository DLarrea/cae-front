import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { AsistenciasComponent } from './asistencias/asistencias.component';
import { UsuariosListComponent } from './admin/usuarios-list/usuarios-list.component';
import { UsuariosAddComponent } from './admin/usuarios-add/usuarios-add.component';
import { UsuariosEditComponent } from './admin/usuarios-edit/usuarios-edit.component';
import { EstudiantesListComponent } from './admin/estudiantes-list/estudiantes-list.component';
import { EstudiantesAddComponent } from './admin/estudiantes-add/estudiantes-add.component';
import { EstudiantesEditComponent } from './admin/estudiantes-edit/estudiantes-edit.component';
import { ProfesoresListComponent } from './admin/profesores-list/profesores-list.component';
import { ProfesoresAddComponent } from './admin/profesores-add/profesores-add.component';
import { ProfesoresEditComponent } from './admin/profesores-edit/profesores-edit.component';
import { MateriasComponent } from './admin/materias/materias.component';
import { CursosComponent } from './admin/cursos/cursos.component';
import { CursosAsociarComponent } from './admin/cursos-asociar/cursos-asociar.component';
import { ResponsablesListComponent } from './admin/responsables-list/responsables-list.component';
import { ResponsablesAddComponent } from './admin/responsables-add/responsables-add.component';
import { ResponsablesEditComponent } from './admin/responsables-edit/responsables-edit.component';

const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        redirectTo: 'inicio',
        pathMatch: 'full'
      },
      {
        path: 'inicio',
        component: HomeComponent
      },
      {
        path: 'asistencias',
        loadChildren: () => import('./asistencias/asistencias.module').then(m => m.AsistenciasModule),
        // path: 'asistencias/:curso',
        // component: AsistenciasComponent
      },
      {
        path: 'admin/usuarios',
        component: UsuariosListComponent
      },
      {
        path: 'admin/usuarios/agregar',
        component: UsuariosAddComponent
      },
      {
        path: 'admin/usuarios/editar/:id',
        component: UsuariosEditComponent
      },
      {
        path: 'admin/estudiantes',
        component: EstudiantesListComponent
      },
      {
        path: 'admin/estudiantes/agregar',
        component: EstudiantesAddComponent
      },
      {
        path: 'admin/estudiantes/editar/:id',
        component: EstudiantesEditComponent
      },
      {
        path: 'admin/profesores',
        component: ProfesoresListComponent
      },
      {
        path: 'admin/profesores/agregar',
        component: ProfesoresAddComponent
      },
      {
        path: 'admin/profesores/editar/:id',
        component: ProfesoresEditComponent
      },
      {
        path: 'admin/responsables',
        component: ResponsablesListComponent
      },
      {
        path: 'admin/responsables/agregar',
        component: ResponsablesAddComponent
      },
      {
        path: 'admin/responsables/editar/:id',
        component: ResponsablesEditComponent
      },
      {
        path: 'admin/materias',
        component: MateriasComponent
      },
      {
        path: 'admin/cursos',
        component: CursosComponent
      },
      {
        path: 'admin/cursos/asociar/:id',
        component: CursosAsociarComponent
      },
      {
        path: '**',
        redirectTo: 'inicio',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
