import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { PagesPrimengModule } from '../primeng/pages-primeng.module';

import { UsuariosListComponent } from './admin/usuarios-list/usuarios-list.component';
import { UsuariosAddComponent } from './admin/usuarios-add/usuarios-add.component';
import { UsuariosEditComponent } from './admin/usuarios-edit/usuarios-edit.component';
import { ProfesoresListComponent } from './admin/profesores-list/profesores-list.component';
import { ProfesoresAddComponent } from './admin/profesores-add/profesores-add.component';
import { ProfesoresEditComponent } from './admin/profesores-edit/profesores-edit.component';
import { CursosComponent } from './admin/cursos/cursos.component';
import { MateriasComponent } from './admin/materias/materias.component';
import { EstudiantesListComponent } from './admin/estudiantes-list/estudiantes-list.component';
import { EstudiantesAddComponent } from './admin/estudiantes-add/estudiantes-add.component';
import { EstudiantesEditComponent } from './admin/estudiantes-edit/estudiantes-edit.component';
import { ConfirmationService, MessageService } from 'primeng/api';
import { CursosAsociarComponent } from './admin/cursos-asociar/cursos-asociar.component';
import { ResponsablesListComponent } from './admin/responsables-list/responsables-list.component';
import { ResponsablesAddComponent } from './admin/responsables-add/responsables-add.component';
import { ResponsablesEditComponent } from './admin/responsables-edit/responsables-edit.component';
import { PersonaCardComponent } from './shared/persona-card/persona-card.component';
import { EmptyCardComponent } from './shared/empty-card/empty-card.component';
import { CardHomeComponent } from './shared/card-home/card-home.component';


@NgModule({
  declarations: [
    PagesComponent,
    HomeComponent,
    NavbarComponent,
    UsuariosListComponent,
    UsuariosAddComponent,
    UsuariosEditComponent,
    ProfesoresListComponent,
    ProfesoresAddComponent,
    ProfesoresEditComponent,
    CursosComponent,
    MateriasComponent,
    EstudiantesListComponent,
    EstudiantesAddComponent,
    EstudiantesEditComponent,
    CursosAsociarComponent,
    ResponsablesListComponent,
    ResponsablesAddComponent,
    ResponsablesEditComponent,
    PersonaCardComponent,
    EmptyCardComponent,
    CardHomeComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PagesPrimengModule,
    PagesRoutingModule
  ],
  providers: [
    ConfirmationService,
    MessageService
  ]
})
export class PagesModule { }
