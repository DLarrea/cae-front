import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { AuthService } from 'src/app/services/auth.service';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { Response } from 'src/app/utils/response';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  cursos: any[] =[];
  estudiantes: any[] =[];
  currentUser: any = null;

  constructor(private usuarioService: UsuariosService, private loader: NgxUiLoaderService, private authService: AuthService){
    this.authService.getUser().subscribe(
      data => {
        if(data != null){
          this.currentUser = data;
          this.currentUser.rol === 'RESPONSABLE' ? this.getEstudiantes(this.currentUser.id) : this.get(this.currentUser.id);
        }
      }
    )
  }


  ngOnInit(): void {}

  get = (id:number) => {
    this.loader.start();
    this.usuarioService.getCursosByUsuarioId(id).subscribe({
      next: (response: Response) => {
        this.cursos = response.result;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.cursos = [];
        this.loader.stop();
      }
    })
  }

  getEstudiantes = (idResponsable:number) => {
    this.loader.start();
    this.usuarioService.getEstudiantesACargo(idResponsable).subscribe({
      next: (response: Response) => {
        this.estudiantes = response.result;
        this.estudiantes.forEach(el => {
          el.estudiante_nombres = String(el.estudiante_nombres).split(' ').shift();
          el.estudiante_apellidos = String(el.estudiante_apellidos).split(' ').shift();
        })
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.estudiantes = [];
        this.loader.stop();
      }
    })
  }

}
