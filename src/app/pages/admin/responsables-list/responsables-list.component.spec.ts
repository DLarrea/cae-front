import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsablesListComponent } from './responsables-list.component';

describe('ResponsablesListComponent', () => {
  let component: ResponsablesListComponent;
  let fixture: ComponentFixture<ResponsablesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResponsablesListComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResponsablesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
