import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { ResponsablesService } from 'src/app/services/responsables.service';
import { Response } from 'src/app/utils/response';
import { itemHome } from 'src/globals';

@Component({
  selector: 'app-responsables-list',
  templateUrl: './responsables-list.component.html',
  styleUrls: ['./responsables-list.component.scss']
})
export class ResponsablesListComponent {
  list: any[] = [];
  items: MenuItem[] = [
    {
      label: 'Responsables',
      routerLink: '/admin/responsables'
    }
  ];
  home: MenuItem = itemHome;

  verEstudiantes: boolean = false;
  listEstudiantes: any[] = [];

  constructor(private responsableService: ResponsablesService, private loader: NgxUiLoaderService, private messageService: MessageService, 
    private confirmationService: ConfirmationService){}

  ngOnInit(): void {
    this.get();
  }

  get = () => {
    this.loader.start();
    this.responsableService.get().subscribe({
      next: (response: Response) => {
        this.list = response.result;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
        this.loader.stop();
      }
    })
  }

  delete = (id:number) => {
    this.responsableService.delete(id).subscribe({
      next: (response: Response) => {
        this.get();
      }, 
      error: (err:HttpErrorResponse) => {
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    })
  } 

  confirm(event: Event, element:any) {
    this.confirmationService.confirm({
      key: 'pages',
      target: event.target ? event.target : undefined,
      message: '¿Eliminar elemento?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      accept: () => {
        this.delete(element.id);
      }
    });
  }

  getEstudiantesACargo = (id : number) => {
    this.loader.start();
    this.listEstudiantes = [];
    this.responsableService.getEstudiantesACargo(id).subscribe({
      next: (response: Response) => {
        this.listEstudiantes = response.result;
        this.verEstudiantes = true;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
        this.loader.stop();
      }
    })
  }
}
