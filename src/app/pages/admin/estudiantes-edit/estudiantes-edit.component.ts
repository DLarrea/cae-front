import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { itemHome, sexosOptions } from 'src/globals';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fieldValidator, formValidator } from 'src/app/utils/formValidator';
import { CursosService } from 'src/app/services/cursos.service';
import { Response } from 'src/app/utils/response';
import { HttpErrorResponse } from '@angular/common/http';
import { EstudiantesService } from 'src/app/services/estudiantes.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ResponsablesService } from 'src/app/services/responsables.service';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-estudiantes-edit',
  templateUrl: './estudiantes-edit.component.html',
  styleUrls: ['./estudiantes-edit.component.scss']
})
export class EstudiantesEditComponent implements OnInit {

  form: FormGroup;
  items: MenuItem[] = [
    {
      label: 'Estudiantes',
      routerLink: '/admin/estudiantes'
    },
    {
      label: 'Editar'
    }
  ];
  home: MenuItem = itemHome;
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  listaSexo = sexosOptions;
  cursos : any[] = [];
  responsables : any[] = [];
  ciudades: any[] = [];
  estudianteId: any = null;
  estudiante: any = null;

  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder, private cursoService: CursosService,
    private estudianteService: EstudiantesService, private messageService: MessageService,
    private loader: NgxUiLoaderService, private responsableService: ResponsablesService,
    private ciudadService: CiudadesService, private router: Router){

      this.form = this.formBuilder.group({
        id: [null, [Validators.required]],
        nombres: [null, [Validators.required]],
        apellidos: [null, [Validators.required]],
        documento: [null, [Validators.required, Validators.pattern("^[0-9]+$")]],
        sexo: [null, ],
        fecha_nacimiento: [null, ],
        ciudad: [null, ],
        barrio: [null, ],
        direccion: [null, ],
        telefono: [null, [Validators.pattern("^595[0-9]{9}$")]],
        email: [null, [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")]],
        curso_id: [null, [Validators.required]],
        responsable_id: [null, [Validators.required]],
        tipo: ['E', [Validators.required]],
      });

    if(this.route.snapshot.paramMap.get('id')){
      this.estudianteId = Number(<string>this.route.snapshot.paramMap.get('id'));  
      this.getEstudiante(); 
    }
  }

  ngOnInit(): void {
    this.getCursos();
    this.getResponsables();
    this.getCiudades();
  }

  getEstudiante = () => {
    this.loader.start();
    this.estudianteService.getById(this.estudianteId).subscribe({
      next: (response: Response) => {
        this.estudiante = response.result;
        this.form.controls['id'].setValue(this.estudiante.id);
        this.form.controls['nombres'].setValue(this.estudiante.nombres);
        this.form.controls['apellidos'].setValue(this.estudiante.apellidos);
        this.form.controls['documento'].setValue(this.estudiante.documento);
        this.form.controls['sexo'].setValue(this.estudiante.sexo);
        this.form.controls['fecha_nacimiento'].setValue(this.estudiante.fecha_nacimiento);
        this.form.controls['telefono'].setValue(this.estudiante.telefono);
        this.form.controls['ciudad'].setValue(this.estudiante.ciudad);
        this.form.controls['barrio'].setValue(this.estudiante.barrio);
        this.form.controls['direccion'].setValue(this.estudiante.direccion);
        this.form.controls['email'].setValue(this.estudiante.email);
        this.form.controls['curso_id'].setValue(this.estudiante.curso_id);
        this.form.controls['responsable_id'].setValue(this.estudiante.responsable_id);
        console.log(this.estudiante);
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.estudiante = null;
        this.loader.stop();
      }
    })
  }

  getCursos = () => {
    this.cursoService.get().subscribe({
      next: (response: Response) => {
        this.cursos = response.result;
      },
      error: (err:HttpErrorResponse) => {
        this.cursos = [];
      }
    })
  }
  
  getResponsables = () => {
    this.responsableService.get().subscribe({
      next: (response: Response) => {
        this.responsables = response.result;
        this.responsables.map(el => el['nombre'] = `${el.documento} | ${el.nombres} ${el.apellidos}`)
      },
      error: (err:HttpErrorResponse) => {
        this.responsables = [];
      }
    })
  }
  
  getCiudades = () => {
    this.ciudadService.get().subscribe({
      next: (response: any) => {
        this.ciudades = response;
        for(let i=0; i<this.ciudades.length;i++){
          this.ciudades[i]['id'] = i + 1;
          this.ciudades[i]['label'] = `${this.ciudades[i]['departamento']}, ${this.ciudades[i]['ciudad']}`;
        }
        console.log(this.ciudades)
      },
      error: (err:HttpErrorResponse) => {
        this.ciudades = [];
      }
    })
  }

  guardarEstudiante = () => {
    if(this.form.valid){
      this.loader.start();
      this.estudianteService.put(this.form.value).subscribe({
        next: (response: Response) => {
          this.router.navigate(['/admin/estudiantes'])
          this.messageService.add({key: 'pages', severity:'success', summary:'Éxito', detail: 'Elemento agregado'});
          this.loader.stop();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loader.stop();
        }
      })
    }
  }
}
