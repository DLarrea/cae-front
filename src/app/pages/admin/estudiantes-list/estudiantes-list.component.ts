import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { EstudiantesService } from 'src/app/services/estudiantes.service';
import { Response } from 'src/app/utils/response';
import { itemHome } from 'src/globals';

@Component({
  selector: 'app-estudiantes-list',
  templateUrl: './estudiantes-list.component.html',
  styleUrls: ['./estudiantes-list.component.scss']
})
export class EstudiantesListComponent implements OnInit {
  
  list: any[] = [];
  items: MenuItem[] = [
    {
      label: 'Estudiantes',
      routerLink: '/admin/estudiantes'
    }
  ];
  home: MenuItem = itemHome

  constructor(private estudianteService: EstudiantesService, private loader: NgxUiLoaderService, private messageService: MessageService, 
    private confirmationService: ConfirmationService){}

  ngOnInit(): void {
    this.get();
  }

  get = () => {
    this.loader.start();
    this.estudianteService.get().subscribe({
      next: (response: Response) => {
        this.list = response.result;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
        this.loader.stop();
      }
    })
  }

  delete = (id:number) => {
    this.estudianteService.delete(id).subscribe({
      next: (response: Response) => {
        this.get();
      }, 
      error: (err:HttpErrorResponse) => {
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    })
  } 

  confirm(event: Event, element:any) {
    this.confirmationService.confirm({
      key: 'pages',
      target: event.target ? event.target : undefined,
      message: '¿Eliminar elemento?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      accept: () => {
        this.delete(element.id);
      }
    });
  }

}
