import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { itemHome, sexosOptions } from 'src/globals';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fieldValidator, formValidator } from 'src/app/utils/formValidator';
import { CursosService } from 'src/app/services/cursos.service';
import { Response } from 'src/app/utils/response';
import { HttpErrorResponse } from '@angular/common/http';
import { EstudiantesService } from 'src/app/services/estudiantes.service';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ResponsablesService } from 'src/app/services/responsables.service';
import { CiudadesService } from 'src/app/services/ciudades.service';

@Component({
  selector: 'app-estudiantes-add',
  templateUrl: './estudiantes-add.component.html',
  styleUrls: ['./estudiantes-add.component.scss']
})
export class EstudiantesAddComponent implements OnInit {

  form: FormGroup;
  items: MenuItem[] = [
    {
      label: 'Estudiantes',
      routerLink: '/admin/estudiantes'
    },
    {
      label: 'Agregar'
    }
  ];
  home: MenuItem = itemHome;
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  listaSexo = sexosOptions;
  cursos : any[] = [];
  responsables : any[] = [];
  ciudades: any[] = [];

  constructor(private formBuilder: FormBuilder, private cursoService: CursosService,
    private estudianteService: EstudiantesService, private messageService: MessageService,
    private loader: NgxUiLoaderService, private responsableService: ResponsablesService,
    private ciudadService: CiudadesService){
    this.form = this.formBuilder.group({
      nombres: [null, [Validators.required]],
      apellidos: [null, [Validators.required]],
      documento: [null, [Validators.required, Validators.pattern("^[0-9]+$")]],
      sexo: [null, ],
      fecha_nacimiento: [null, ],
      ciudad: [null, ],
      barrio: [null, ],
      direccion: [null, ],
      telefono: [null, [Validators.pattern("^595[0-9]{9}$")]],
      email: [null, [Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")]],
      curso_id: [null, [Validators.required]],
      responsable_id: [null, [Validators.required]],
      tipo: ['E', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getCursos();
    this.getResponsables();
    this.getCiudades();
  }

  guardarEstudiante = () => {
    if(this.form.valid){
      this.loader.start();
      this.estudianteService.post(this.form.value).subscribe({
        next: (response: Response) => {
          this.messageService.add({key: 'pages', severity:'success', summary:'Éxito', detail: 'Elemento agregado'});
          this.form.reset();
          this.loader.stop();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loader.stop();
        }
      })
    }
  }
  
  getCursos = () => {
    this.cursoService.get().subscribe({
      next: (response: Response) => {
        this.cursos = response.result;
      },
      error: (err:HttpErrorResponse) => {
        this.cursos = [];
      }
    })
  }
  
  getResponsables = () => {
    this.responsableService.get().subscribe({
      next: (response: Response) => {
        this.responsables = response.result;
        this.responsables.forEach(el => el['nombre'] = `${el.documento} | ${el.nombres} ${el.apellidos}`)
      },
      error: (err:HttpErrorResponse) => {
        this.responsables = [];
      }
    })
  }
  
  getCiudades = () => {
    this.ciudadService.get().subscribe({
      next: (response: any) => {
        this.ciudades = response;
        for(let i=0; i<this.ciudades.length;i++){
          this.ciudades[i]['id'] = i + 1;
          this.ciudades[i]['label'] = `${this.ciudades[i]['departamento']}, ${this.ciudades[i]['ciudad']}`;
        }
        console.log(this.ciudades)
      },
      error: (err:HttpErrorResponse) => {
        this.ciudades = [];
      }
    })
  }
}
