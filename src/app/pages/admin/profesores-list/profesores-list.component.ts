import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { ProfesoresService } from 'src/app/services/profesores.service';
import { Response } from 'src/app/utils/response';
import { itemHome } from 'src/globals';

@Component({
  selector: 'app-profesores-list',
  templateUrl: './profesores-list.component.html',
  styleUrls: ['./profesores-list.component.scss']
})
export class ProfesoresListComponent implements OnInit {
  list: any[] = [];
  items: MenuItem[] = [
    {
      label: 'Profesores',
      routerLink: '/admin/profesores'
    }
  ];
  home: MenuItem = itemHome

  constructor(private profesorService: ProfesoresService, private loader: NgxUiLoaderService, private messageService: MessageService, 
    private confirmationService: ConfirmationService){}

  ngOnInit(): void {
    this.get();
  }

  get = () => {
    this.loader.start();
    this.profesorService.get().subscribe({
      next: (response: Response) => {
        this.list = response.result;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
        this.loader.stop();
      }
    })
  }

  delete = (id:number) => {
    this.profesorService.delete(id).subscribe({
      next: (response: Response) => {
        this.get();
      }, 
      error: (err:HttpErrorResponse) => {
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    })
  } 

  confirm(event: Event, element:any) {
    this.confirmationService.confirm({
      key: 'pages',
      target: event.target ? event.target : undefined,
      message: '¿Eliminar elemento?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      accept: () => {
        this.delete(element.id);
      }
    });
  }

}
