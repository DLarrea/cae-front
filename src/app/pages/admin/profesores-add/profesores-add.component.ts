import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { itemHome, sexosOptions } from 'src/globals';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fieldValidator, formValidator } from 'src/app/utils/formValidator';
import { Response } from 'src/app/utils/response';
import { HttpErrorResponse } from '@angular/common/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { ProfesoresService } from 'src/app/services/profesores.service';


@Component({
  selector: 'app-profesores-add',
  templateUrl: './profesores-add.component.html',
  styleUrls: ['./profesores-add.component.scss']
})
export class ProfesoresAddComponent implements OnInit {
  form: FormGroup;
  items: MenuItem[] = [
    {
      label: 'Profesores',
      routerLink: '/admin/profesores'
    },
    {
      label: 'Agregar'
    }
  ];
  home: MenuItem = itemHome;
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  listaSexo = sexosOptions;
  cursos : any[] = [];
  responsables : any[] = [];
  ciudades: any[] = [];

  constructor(private formBuilder: FormBuilder,
    private messageService: MessageService,
    private loader: NgxUiLoaderService, 
    private profesorService: ProfesoresService,
    private ciudadService: CiudadesService){
    this.form = this.formBuilder.group({
      nombres: [null, [Validators.required]],
      apellidos: [null, [Validators.required]],
      documento: [null, [Validators.required, Validators.pattern("^[0-9]+$")]],
      sexo: [null, ],
      fecha_nacimiento: [null, ],
      ciudad: [null, ],
      barrio: [null, ],
      direccion: [null, ],
      telefono: [null, [Validators.pattern("^595[0-9]{9}$")]],
      email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")]],
      tipo: ['P', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.getCiudades();
  }

  guardarResponsable = () => {
    if(this.form.valid){
      this.loader.start();
      this.profesorService.post(this.form.value).subscribe({
        next: (response: Response) => {
          this.messageService.add({key: 'pages', severity:'success', summary:'Éxito', detail: 'Elemento agregado'});
          this.form.reset();
          this.loader.stop();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loader.stop();
        }
      })
    }
  }
  
  getCiudades = () => {
    this.ciudadService.get().subscribe({
      next: (response: any) => {
        this.ciudades = response;
        for(let i=0; i<this.ciudades.length;i++){
          this.ciudades[i]['id'] = i + 1;
          this.ciudades[i]['label'] = `${this.ciudades[i]['departamento']}, ${this.ciudades[i]['ciudad']}`;
        }
        console.log(this.ciudades)
      },
      error: (err:HttpErrorResponse) => {
        this.ciudades = [];
      }
    })
  }
}
