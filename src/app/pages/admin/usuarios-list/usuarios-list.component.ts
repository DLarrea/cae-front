import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { UsuariosService } from 'src/app/services/usuarios.service';
import { Response } from 'src/app/utils/response';
import { itemHome } from 'src/globals';

@Component({
  selector: 'app-usuarios-list',
  templateUrl: './usuarios-list.component.html',
  styleUrls: ['./usuarios-list.component.scss']
})
export class UsuariosListComponent implements OnInit {
  list: any[] = [];
  items: MenuItem[] = [
    {
      label: 'Usuarios',
      routerLink: '/admin/usuarios'
    }
  ];
  home: MenuItem = itemHome

  constructor(private usuarioService: UsuariosService, private loader: NgxUiLoaderService, private messageService: MessageService, 
    private confirmationService: ConfirmationService){}

  ngOnInit(): void {
    this.get();
  }

  get = () => {
    this.loader.start();
    this.usuarioService.get().subscribe({
      next: (response: Response) => {
        this.list = response.result;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
        this.loader.stop();
      }
    })
  }

  cambiarEstado = (form : any) => {
    this.usuarioService.cambiarEstado(form).subscribe({
      next: (response: Response) => {
        this.get();
      }, 
      error: (err:HttpErrorResponse) => {
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    })
  } 

  confirmInactivo(event: Event, element:any) {
    this.confirmationService.confirm({
      key: 'pages',
      target: event.target ? event.target : undefined,
      message: '¿Inactivar usuario?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      accept: () => {
        this.cambiarEstado({id : element.id, activo : 0});
      }
    });
  }
  
  confirmActivo(event: Event, element:any) {
    this.confirmationService.confirm({
      key: 'pages',
      target: event.target ? event.target : undefined,
      message: '¿Activar usuario?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      accept: () => {
        this.cambiarEstado({id : element.id, activo : 1});
      }
    });
  }
}
