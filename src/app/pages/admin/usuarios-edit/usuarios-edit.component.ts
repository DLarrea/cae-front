import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { itemHome, sexosOptions } from 'src/globals';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fieldValidator, formValidator } from 'src/app/utils/formValidator';
import { Response } from 'src/app/utils/response';
import { HttpErrorResponse } from '@angular/common/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-usuarios-edit',
  templateUrl: './usuarios-edit.component.html',
  styleUrls: ['./usuarios-edit.component.scss']
})
export class UsuariosEditComponent implements OnInit {
  form: FormGroup;
  items: MenuItem[] = [
    {
      label: 'Usuarios',
      routerLink: '/admin/usuarios'
    },
    {
      label: 'Editar'
    }
  ];
  home: MenuItem = itemHome;
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  listaSexo = sexosOptions;
  ciudades: any[] = [];
  usuarioId: any = null;
  usuario: any = null;

  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder,
    private messageService: MessageService,
    private loader: NgxUiLoaderService, private usuarioService: UsuariosService,
    private ciudadService: CiudadesService, private router: Router){

      this.form = this.formBuilder.group({
        id: [null, [Validators.required]],
        nombres: [null, [Validators.required]],
        apellidos: [null, [Validators.required]],
        documento: [null, [Validators.required, Validators.pattern("^[0-9]+$")]],
        sexo: [null, ],
        fecha_nacimiento: [null, ],
        ciudad: [null, ],
        barrio: [null, ],
        direccion: [null, ],
        telefono: [null, [Validators.pattern("^595[0-9]{9}$")]],
        email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")]],
        tipo: ['ADMIN', [Validators.required]],
      });

    if(this.route.snapshot.paramMap.get('id')){
      this.usuarioId = Number(<string>this.route.snapshot.paramMap.get('id'));  
      this.getProfesor(); 
    }
  }

  ngOnInit(): void {
    this.getCiudades();
  }

  getProfesor = () => {
    this.loader.start();
    this.usuarioService.getById(this.usuarioId).subscribe({
      next: (response: Response) => {
        this.usuario = response.result;
        this.form.controls['id'].setValue(this.usuario.id);
        this.form.controls['nombres'].setValue(this.usuario.nombres);
        this.form.controls['apellidos'].setValue(this.usuario.apellidos);
        this.form.controls['documento'].setValue(this.usuario.documento);
        this.form.controls['sexo'].setValue(this.usuario.sexo);
        this.form.controls['fecha_nacimiento'].setValue(this.usuario.fecha_nacimiento);
        this.form.controls['telefono'].setValue(this.usuario.telefono);
        this.form.controls['ciudad'].setValue(this.usuario.ciudad);
        this.form.controls['barrio'].setValue(this.usuario.barrio);
        this.form.controls['direccion'].setValue(this.usuario.direccion);
        this.form.controls['email'].setValue(this.usuario.email);
        console.log(this.usuario);
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.usuario = null;
        this.loader.stop();
      }
    })
  }

  
  getCiudades = () => {
    this.ciudadService.get().subscribe({
      next: (response: any) => {
        this.ciudades = response;
        for(let i=0; i<this.ciudades.length;i++){
          this.ciudades[i]['id'] = i + 1;
          this.ciudades[i]['label'] = `${this.ciudades[i]['departamento']}, ${this.ciudades[i]['ciudad']}`;
        }
        console.log(this.ciudades)
      },
      error: (err:HttpErrorResponse) => {
        this.ciudades = [];
      }
    })
  }

  guardarUsuario = () => {
    if(this.form.valid){
      this.loader.start();
      this.usuarioService.put(this.form.value).subscribe({
        next: (response: Response) => {
          this.router.navigate(['/admin/usuarios'])
          this.messageService.add({key: 'pages', severity:'success', summary:'Éxito', detail: 'Elemento agregado'});
          this.loader.stop();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loader.stop();
        }
      })
    }
  }
}
