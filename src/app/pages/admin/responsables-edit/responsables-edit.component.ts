import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { itemHome, sexosOptions } from 'src/globals';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { fieldValidator, formValidator } from 'src/app/utils/formValidator';
import { Response } from 'src/app/utils/response';
import { HttpErrorResponse } from '@angular/common/http';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ResponsablesService } from 'src/app/services/responsables.service';
import { CiudadesService } from 'src/app/services/ciudades.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-responsables-edit',
  templateUrl: './responsables-edit.component.html',
  styleUrls: ['./responsables-edit.component.scss']
})
export class ResponsablesEditComponent implements OnInit {

  form: FormGroup;
  items: MenuItem[] = [
    {
      label: 'Responsables',
      routerLink: '/admin/responsables'
    },
    {
      label: 'Editar'
    }
  ];
  home: MenuItem = itemHome;
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  listaSexo = sexosOptions;
  ciudades: any[] = [];
  responsableId: any = null;
  responsable: any = null;

  constructor(private route: ActivatedRoute, private formBuilder: FormBuilder,
    private messageService: MessageService,
    private loader: NgxUiLoaderService, private responsableService: ResponsablesService,
    private ciudadService: CiudadesService, private router: Router){

      this.form = this.formBuilder.group({
        id: [null, [Validators.required]],
        nombres: [null, [Validators.required]],
        apellidos: [null, [Validators.required]],
        documento: [null, [Validators.required, Validators.pattern("^[0-9]+$")]],
        sexo: [null, ],
        fecha_nacimiento: [null, ],
        ciudad: [null, ],
        barrio: [null, ],
        direccion: [null, ],
        telefono: [null, [Validators.pattern("^595[0-9]{9}$")]],
        email: [null, [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$")]],
        tipo: ['R', [Validators.required]],
      });

    if(this.route.snapshot.paramMap.get('id')){
      this.responsableId = Number(<string>this.route.snapshot.paramMap.get('id'));  
      this.getResponsable(); 
    }
  }

  ngOnInit(): void {
    this.getCiudades();
  }

  getResponsable = () => {
    this.loader.start();
    this.responsableService.getById(this.responsableId).subscribe({
      next: (response: Response) => {
        this.responsable = response.result;
        this.form.controls['id'].setValue(this.responsable.id);
        this.form.controls['nombres'].setValue(this.responsable.nombres);
        this.form.controls['apellidos'].setValue(this.responsable.apellidos);
        this.form.controls['documento'].setValue(this.responsable.documento);
        this.form.controls['sexo'].setValue(this.responsable.sexo);
        this.form.controls['fecha_nacimiento'].setValue(this.responsable.fecha_nacimiento);
        this.form.controls['telefono'].setValue(this.responsable.telefono);
        this.form.controls['ciudad'].setValue(this.responsable.ciudad);
        this.form.controls['barrio'].setValue(this.responsable.barrio);
        this.form.controls['direccion'].setValue(this.responsable.direccion);
        this.form.controls['email'].setValue(this.responsable.email);
        console.log(this.responsable);
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.responsable = null;
        this.loader.stop();
      }
    })
  }

  
  getCiudades = () => {
    this.ciudadService.get().subscribe({
      next: (response: any) => {
        this.ciudades = response;
        for(let i=0; i<this.ciudades.length;i++){
          this.ciudades[i]['id'] = i + 1;
          this.ciudades[i]['label'] = `${this.ciudades[i]['departamento']}, ${this.ciudades[i]['ciudad']}`;
        }
        console.log(this.ciudades)
      },
      error: (err:HttpErrorResponse) => {
        this.ciudades = [];
      }
    })
  }

  guardarResponsable = () => {
    if(this.form.valid){
      this.loader.start();
      this.responsableService.put(this.form.value).subscribe({
        next: (response: Response) => {
          this.router.navigate(['/admin/responsables'])
          this.messageService.add({key: 'pages', severity:'success', summary:'Éxito', detail: 'Elemento agregado'});
          this.loader.stop();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
          this.loader.stop();
        }
      })
    }
  }
}
