import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { Response } from 'src/app/utils/response';
import { fieldValidator, formValidator } from 'src/app/utils/formValidator';
import { CursosService } from 'src/app/services/cursos.service';
import { itemHome } from 'src/globals';
import { MateriasService } from 'src/app/services/materias.service';

@Component({
  selector: 'app-materias',
  templateUrl: './materias.component.html',
  styleUrls: ['./materias.component.scss']
})
export class MateriasComponent implements OnInit {
  list: any[] = [];
  formAdd : FormGroup;
  formEdit : FormGroup;
  elementDelete: any = null;
  elementAddShow: boolean = false;
  elementEditShow: boolean = false;
  elementAddLoading: boolean = false;
  elementEditLoading: boolean = false;
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  items: MenuItem[] = [
    {
      label: 'Materias',
      routerLink: '/admin/materias'
    }
  ];
  home: MenuItem = itemHome

  constructor(private materiaService: MateriasService, private formBuilder: FormBuilder, private messageService: MessageService, 
    private confirmationService: ConfirmationService, private loader: NgxUiLoaderService) {
    this.formAdd = this.formBuilder.group({
      nombre: ['', [Validators.required]],
    });
    this.formEdit = this.formBuilder.group({
      id: [null, [Validators.required]],
      nombre: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this.get();
  }

  get = () => {
    this.loader.start();
    this.materiaService.get().subscribe({
      next: (response: Response) => {
        this.list = response.result;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
        this.loader.stop();
      }
    })
  }


  add = () => {
    if(this.formAdd.valid){
      this.elementAddLoading = true;
      this.materiaService.post(this.formAdd.value).subscribe({
        next: (response: Response) => {
          this.get();
          this.messageService.add({key: 'pages', severity:'success', summary:'Éxito', detail: 'Elemento agregado'});
          this.elementAddShow = false;
          this.elementAddLoading = false;
          this.formAdd.reset();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          this.elementAddLoading = false;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
        }
      })
    }
  }

  edit = () => {
    if(this.formEdit.valid){
      this.elementEditLoading = true;
      this.materiaService.put(this.formEdit.value).subscribe({
        next: (response: Response) => {
          this.get();
          this.messageService.add({key: 'pages', severity:'success', summary:'Éxito', detail: 'Elemento editado'});
          this.elementEditShow = false;
          this.elementEditLoading = false;
          this.formEdit.reset();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          this.elementEditLoading = false;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
        }
      })
    }
  }

  delete = (id:number) => {
    this.materiaService.delete(id).subscribe({
      next: (response: Response) => {
        this.get();
      }, 
      error: (err:HttpErrorResponse) => {
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    })
  } 

  setEdit(element:any){
    this.formEdit.controls['nombre'].setValue(element.nombre);
    this.formEdit.controls['id'].setValue(element.id);
    this.elementEditShow = true;
  }

  confirm(event: Event, element:any) {
    this.confirmationService.confirm({
      key: 'pages',
      target: event.target ? event.target : undefined,
      message: '¿Eliminar elemento?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      accept: () => {
        this.delete(element.id);
      }
    });
  }
}
