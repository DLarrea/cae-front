import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { CursosService } from 'src/app/services/cursos.service';
import { MateriasService } from 'src/app/services/materias.service';
import { ProfesoresService } from 'src/app/services/profesores.service';
import { fieldValidator, formValidator } from 'src/app/utils/formValidator';
import { Response } from 'src/app/utils/response';
import { itemHome } from 'src/globals';

@Component({
  selector: 'app-cursos-asociar',
  templateUrl: './cursos-asociar.component.html',
  styleUrls: ['./cursos-asociar.component.scss']
})
export class CursosAsociarComponent implements OnInit {
  items: MenuItem[] = [
    {
      label: 'Cursos',
      routerLink: '/admin/cursos'
    },
    {
      label: 'Asociar materias/profesores'
    }
  ];
  home: MenuItem = itemHome;
  cursoId: any = null;
  curso: any = null;
  materias: any[] = [];
  profesores: any[] = [];
  formValidator = formValidator;
  fieldValidator = fieldValidator;
  formAdd: FormGroup;
  elementAddShow: boolean = false;
  list: any[] = [];

  constructor(private route: ActivatedRoute, private loader: NgxUiLoaderService,
    private cursoService: CursosService, private materiaService: MateriasService,
    private profesorService: ProfesoresService, private fb: FormBuilder, 
    private messageService: MessageService, private confirmationService: ConfirmationService){
    if(this.route.snapshot.paramMap.get('id')){
      this.cursoId = Number(<string>this.route.snapshot.paramMap.get('id'));  
      this.getEstudiante(); 
    }

    this.formAdd = this.fb.group({
      materia_id: [null, Validators.required],
      persona_id: [null, [Validators.required]],
      curso_id: [null,]
    });
  }

  ngOnInit(): void {
    this.get();
    this.getMaterias();
    this.getProfesores();
  }

  getEstudiante = () => {
    this.loader.start();
    this.cursoService.getById(this.cursoId).subscribe({
      next: (response: Response) => {
        this.curso = response.result;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.curso = null;
        this.loader.stop();
      }
    })
  }

  getMaterias = () => {
    this.materiaService.get().subscribe({
      next: (response: Response) => {
        this.materias = response.result;
      },
      error: (err:HttpErrorResponse) => {
        this.materias = [];
      }
    })
  }

  getProfesores = () => {
    this.profesorService.get().subscribe({
      next: (response: Response) => {
        this.profesores = response.result;
        this.profesores.forEach(el => el['nombre'] = `${el.documento} | ${el.nombres} ${el.apellidos}`)
      },
      error: (err:HttpErrorResponse) => {
        this.profesores = [];
      }
    })
  }

  add = () => {
    this.formAdd.controls['curso_id'].setValue(this.cursoId);
    if(this.formAdd.valid){
      this.cursoService.postAsociar(this.formAdd.value).subscribe({
        next: (response: Response) => {
          this.get();
          this.messageService.add({key: 'pages', severity:'success', summary:'Éxito', detail: 'Elemento agregado'});
          this.elementAddShow = false;
          this.formAdd.reset();
        },
        error: (err:HttpErrorResponse) => {
          let error: Response = err.error;
          if(Array.isArray(error.message)){
            error.message.forEach(m => {
              this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
            })
          } else {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
          }
        }
      })
    }
  }

  get = () => {
    this.loader.start();
    this.cursoService.getMateriaProfesorByCursoId(this.cursoId).subscribe({
      next: (response: Response) => {
        this.list = response.result;
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.list = [];
        this.loader.stop();
      }
    })
  }

  delete = (id:number) => {
    this.cursoService.deleteCursoMateriaProfesor(id).subscribe({
      next: (response: Response) => {
        this.get();
      }, 
      error: (err:HttpErrorResponse) => {
        let error: Response = err.error;
        if(Array.isArray(error.message)){
          error.message.forEach(m => {
            this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: m?.msg || 'Error en el servidor'});
          })
        } else {
          this.messageService.add({key: 'pages', severity:'error', summary:'Error', detail: error.message || 'Error en el servidor'});
        }
      }
    })
  } 

  confirm(event: Event, element:any) {
    this.confirmationService.confirm({
      key: 'pages',
      target: event.target ? event.target : undefined,
      message: '¿Eliminar elemento?',
      icon: 'pi pi-exclamation-triangle',
      acceptLabel: 'Si',
      accept: () => {
        this.delete(element.id);
      }
    });
  }
}
