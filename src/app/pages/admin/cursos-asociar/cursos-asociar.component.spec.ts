import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CursosAsociarComponent } from './cursos-asociar.component';

describe('CursosAsociarComponent', () => {
  let component: CursosAsociarComponent;
  let fixture: ComponentFixture<CursosAsociarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CursosAsociarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CursosAsociarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
