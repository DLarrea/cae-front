import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsablesAddComponent } from './responsables-add.component';

describe('ResponsablesAddComponent', () => {
  let component: ResponsablesAddComponent;
  let fixture: ComponentFixture<ResponsablesAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResponsablesAddComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ResponsablesAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
