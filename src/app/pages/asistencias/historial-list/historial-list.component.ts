import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as dayjs from 'dayjs';
import { Workbook } from 'exceljs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { MateriasService } from 'src/app/services/materias.service';
import { Response } from 'src/app/utils/response';
import { itemHome } from 'src/globals';
import * as fs from 'file-saver';

@Component({
  selector: 'app-historial-list',
  templateUrl: './historial-list.component.html',
  styleUrls: ['./historial-list.component.scss']
})
export class HistorialListComponent {

  asistencias: any[] =[];
  notas: any[] =[];
  materia: any = null;
  curso: any = null;
  profesor: any = null;
  currentUser: any = null;
  items: MenuItem[] = [];
  home: MenuItem = itemHome;

  constructor(private route:ActivatedRoute, private loader: NgxUiLoaderService, 
    private authService: AuthService, private materiaService: MateriasService){
      this.authService.getUser().subscribe(
        data => {
          if(data != null){
            this.currentUser = data;
            const cursoId = this.route.snapshot.paramMap.get('curso');
            const materiaId = this.route.snapshot.paramMap.get('materia');
            this.get(Number(cursoId), Number(materiaId), Number(this.currentUser.id));
          }
        }
      )
  }

  get = (cursoId: number, materiaId: number, usuarioId: number) => {
    this.loader.start();
    this.materiaService.getHistorialAsistencias(cursoId, materiaId, usuarioId).subscribe({
      next: (response: Response) => {
        this.asistencias = response.result.asistencias;
        this.materia = response.result.materia;
        this.curso = response.result.curso;
        this.profesor = response.result.profesor;
        this.notas = response.result.notas;

        this.asistencias.forEach((el) => {
          el.fecha = dayjs(el.fecha).format('DD/MM/YYYY');
        });

        this.items = [
          {
            label: this.curso.nombre,
            routerLink: `/asistencias/${this.curso.id}`
          },
          {
            label: this.materia.nombre,
            routerLink: `/asistencias/${this.curso.id}/asistencia/${this.materia.id}`
          },
          {
            label: 'Historial'
          }
        ]

        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.asistencias = [];
        this.loader.stop();
      }
    })
  }

  excel = () => {
    this.loader.start();
    let workbook = new Workbook();

    // Asistencias

    let ws = workbook.addWorksheet('Reporte');
    let titleRow = ws.addRow([`Reporte de asistencias`]);
    titleRow.height = 35;
    titleRow.font = { name: 'Calibri', family: 4, size: 16, bold: true, color: { argb: "00cf8d2e" } };

    // Profesor
    let profesorRow = ws.addRow(['Profesor', `${this.profesor?.nombres} ${this.profesor?.apellidos}`]);
    profesorRow.getCell(1).font = { name: 'Calibri', family: 4, bold: true, size: 10 };
    profesorRow.getCell(2).font = { name: 'Calibri', family: 4, bold: true, color: { argb: "00cf8d2e" }, size: 10 };

    // Cargo
    let cursoRow = ws.addRow(['Curso', this.curso?.nombre])
    cursoRow.getCell(1).font = { name: 'Calibri', family: 4, bold: true, size: 10 };
    cursoRow.getCell(2).font = { name: 'Calibri', family: 4, bold: true, color: { argb: "00cf8d2e" }, size: 10 };
    
    // Cargo
    let materiaRow = ws.addRow(['Materia', this.materia?.nombre])
    materiaRow.getCell(1).font = { name: 'Calibri', family: 4, bold: true, size: 10 };
    materiaRow.getCell(2).font = { name: 'Calibri', family: 4, bold: true, color: { argb: "00cf8d2e" }, size: 10 };

    // Generacion del reporte
    let repGenRow = ws.addRow(['Fecha de generación', dayjs().format('DD/MM/YYYY HH:mm:ss')]);
    repGenRow.getCell(1).font = { name: 'Calibri', family: 4, bold: true, size: 10 };
    repGenRow.getCell(2).font = { name: 'Calibri', family: 4, bold: true, color: { argb: "00cf8d2e" }, size: 10 };

    let columns: string[] = ['Nombres', 'Apellidos', 'Fecha', 'Asistencia', 'Justificación'];
    let columnsRow = ws.addRow(columns);
    columnsRow.font = { name: 'Calibri', family: 4, bold: true, size: 10 };

    ws.mergeCells(`A1:${ws.getRow(1).getCell(columns.length).address}`);
    ws.mergeCells(`B2:${ws.getRow(2).getCell(columns.length).address}`);
    ws.mergeCells(`B3:${ws.getRow(3).getCell(columns.length).address}`);
    ws.mergeCells(`B4:${ws.getRow(4).getCell(columns.length).address}`);
    ws.mergeCells(`B5:${ws.getRow(5).getCell(columns.length).address}`);

    for(let i = 1; i <= columns.length; i++) ws.getColumn(i).width = 25;

    this.asistencias.filter(el => !el.ticket_nc).forEach(el => {
      ws.addRow([
        el.estudiante_nombres,
        el.estudiante_apellidos,
        el.fecha,
        el.asistencia == 1 ? 'PRESENTE':'AUSENTE',
        el.asistencia_justificacion
      ]);
    })


    //notas
    let ws2 = workbook.addWorksheet('Reporte notas');
    let titleRowNota = ws2.addRow([`Reporte de notas`]);
    titleRowNota.height = 35;
    titleRowNota.font = { name: 'Calibri', family: 4, size: 16, bold: true, color: { argb: "00cf8d2e" } };

    // Profesor
    let profesorRowNota = ws2.addRow(['Profesor', `${this.profesor?.nombres} ${this.profesor?.apellidos}`]);
    profesorRowNota.getCell(1).font = { name: 'Calibri', family: 4, bold: true, size: 10 };
    profesorRowNota.getCell(2).font = { name: 'Calibri', family: 4, bold: true, color: { argb: "00cf8d2e" }, size: 10 };

    // Curso
    let cursoRowNota = ws2.addRow(['Curso', this.curso?.nombre])
    cursoRowNota.getCell(1).font = { name: 'Calibri', family: 4, bold: true, size: 10 };
    cursoRowNota.getCell(2).font = { name: 'Calibri', family: 4, bold: true, color: { argb: "00cf8d2e" }, size: 10 };

    // Materia
    let materiaRowNota = ws2.addRow(['Materia', this.materia?.nombre])
    materiaRowNota.getCell(1).font = { name: 'Calibri', family: 4, bold: true, size: 10 };
    materiaRowNota.getCell(2).font = { name: 'Calibri', family: 4, bold: true, color: { argb: "00cf8d2e" }, size: 10 };

    // Generacion del reporte
    let genRepNota = ws2.addRow(['Fecha de generación', dayjs().format('DD/MM/YYYY HH:mm:ss')]);
    genRepNota.getCell(1).font = { name: 'Calibri', family: 4, bold: true, size: 10 };
    genRepNota.getCell(2).font = { name: 'Calibri', family: 4, bold: true, color: { argb: "00cf8d2e" }, size: 10 };

    let columnsNota: string[] = ['Notas', 'Fecha'];
    let columnsNotaRow = ws2.addRow(columnsNota);
    columnsNotaRow.font = { name: 'Calibri', family: 4, bold: true, size: 10 };

    ws2.mergeCells(`A1:${ws2.getRow(1).getCell(columns.length).address}`);
    ws2.mergeCells(`B2:${ws2.getRow(2).getCell(columns.length).address}`);
    ws2.mergeCells(`B3:${ws2.getRow(3).getCell(columns.length).address}`);
    ws2.mergeCells(`B4:${ws2.getRow(4).getCell(columns.length).address}`);
    ws2.mergeCells(`B5:${ws2.getRow(5).getCell(columns.length).address}`);

    for(let i = 1; i <= columns.length; i++) ws2.getColumn(i).width = 25;

    this.notas.filter(el => !el.ticket_nc).forEach(el => {
      ws2.addRow([
        el.nota,
        dayjs(el.fecha_actualizacion).format('DD/MM/YYYY HH:mm:ss')
      ]);
    })

    workbook.xlsx.writeBuffer().then((data) => {
      const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
      fs.saveAs(blob, `Reporte_Asistencias_${new Date().getTime()}.xlsx`);
    });

    this.loader.stop();
  }
}
