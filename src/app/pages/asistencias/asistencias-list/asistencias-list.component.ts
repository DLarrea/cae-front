import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { EstudiantesService } from 'src/app/services/estudiantes.service';
import { Response } from 'src/app/utils/response';
import { environment } from 'src/environments/environment.development';
import { itemHome } from 'src/globals';
import * as dayjs from 'dayjs';

@Component({
  selector: 'app-asistencias-list',
  templateUrl: './asistencias-list.component.html',
  styleUrls: ['./asistencias-list.component.scss']
})
export class AsistenciasListComponent {
  estudiantes: any[] =[];
  materia: any = null;
  curso: any = null;
  profesor: any = null;
  currentUser: any = null;
  items: MenuItem[] = [];
  home: MenuItem = itemHome;
  estudiantesPresentes: any[] = [];
  showConfirmarAsistencia: boolean = false;
  env = environment;
  nota: string = '';
  fechaActual = dayjs().format('DD/MM/YYYY');

  constructor(private estudianteService: EstudiantesService, private route:ActivatedRoute, private loader: NgxUiLoaderService, private authService: AuthService, private router: Router){
    this.authService.getUser().subscribe(
      data => {
        if(data != null){
          this.currentUser = data;
          const cursoId = this.route.snapshot.paramMap.get('curso');
          const materiaId = this.route.snapshot.paramMap.get('materia');
          this.get(Number(cursoId), Number(materiaId), Number(this.currentUser.id));
        }
      }
    )
  }


  ngOnInit(): void {
    
  }
  

  get = (cursoId: number, materiaId: number, usuarioId: number) => {
    this.loader.start();
    this.estudianteService.getAsistenciasEstudiantesById(cursoId, materiaId, usuarioId).subscribe({
      next: (response: Response) => {
        this.estudiantes = response.result.estudiantes;
        this.materia = response.result.materia;
        this.curso = response.result.curso;
        this.profesor = response.result.profesor;

        this.items = [
          {
            label: this.curso.nombre,
            routerLink: `/asistencias/${this.curso.id}`
          },
          {
            label: this.materia.nombre
          }
        ]

        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.estudiantes = [];
        this.loader.stop();
      }
    })
  }

  confirmar = () => {
    let asistencias = this.estudiantesPresentes.map((el) => {
      return {
        estudiante_id : el.estudiante_id,
        asistencia: 1
      }
    });

    let ausentes = this.estudiantes.filter((el) => {
      return typeof asistencias.find(a => a.estudiante_id == el.estudiante_id) === 'undefined';
    }).map((el) => {
      return {
        estudiante_id : el.estudiante_id,
        asistencia: 0
      }
    });

    asistencias = [...asistencias, ...ausentes]

    let data = {
      asistencias,
      nota: this.nota,
      curso : this.curso.id,
      materia : this.materia.id
    }

    this.loader.start();
    this.estudianteService.registrarAsistencia(data).subscribe({
      next: (response: Response) => {
        this.router.navigate([`/asistencias/${this.curso.id}`])
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.showConfirmarAsistencia = false;
        this.loader.stop();
      }
    })
  }
}
