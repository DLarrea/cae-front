import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AsistenciasEstudianteComponent } from './asistencias-estudiante.component';

describe('AsistenciasEstudianteComponent', () => {
  let component: AsistenciasEstudianteComponent;
  let fixture: ComponentFixture<AsistenciasEstudianteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AsistenciasEstudianteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AsistenciasEstudianteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
