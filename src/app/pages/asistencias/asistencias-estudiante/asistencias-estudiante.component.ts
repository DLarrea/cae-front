import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as dayjs from 'dayjs';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { EstudiantesService } from 'src/app/services/estudiantes.service';
import { Response } from 'src/app/utils/response';
import { itemHome } from 'src/globals';
import { fieldValidator, formValidator } from 'src/app/utils/formValidator';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-asistencias-estudiante',
  templateUrl: './asistencias-estudiante.component.html',
  styleUrls: ['./asistencias-estudiante.component.scss']
})
export class AsistenciasEstudianteComponent implements OnInit {

  asistencias: any[] =[];
  currentUser: any = null;
  estudianteId: any = null;
  estudiante: any = null;
  items: MenuItem[] = [];
  home: MenuItem = itemHome;
  elementEditShow: boolean = false;
  formEdit : FormGroup;
  elementEditLoading: boolean = false;
  formValidator = formValidator;
  fieldValidator = fieldValidator;

  constructor(private route:ActivatedRoute, private loader: NgxUiLoaderService, 
    private authService: AuthService, private estudianteService: EstudiantesService,
    private formBuilder: FormBuilder){
      this.authService.getUser().subscribe(
        data => {
          if(data != null){
            this.currentUser = data;
            this.estudianteId = this.route.snapshot.paramMap.get('id');
            this.get(Number(this.estudianteId));
          }
        }
      )

      this.formEdit = this.formBuilder.group({
        id: [null, [Validators.required]],
        asistencia_justificacion: [null, [Validators.required]],
      });
  }

  ngOnInit(): void {}

  get = (estudianteId:number) => {
    this.loader.start();
    this.estudianteService.getAsistenciasByEstudianteId(estudianteId).subscribe({
      next: (response: Response) => {
        this.asistencias = response.result.asistencias;
        this.estudiante = response.result.estudiante;

        this.asistencias.forEach((el) => {
          el.fecha = dayjs(el.fecha).format('DD/MM/YYYY');
        });

        this.items = [
          {
            label: 'Historial de asistencias',
          }
        ]

        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.asistencias = [];
        this.loader.stop();
      }
    })
  }

  edit = () => {
    if(this.formEdit.valid){
      this.loader.start();
      this.elementEditLoading = true;
      this.estudianteService.justificarAusenciaEstudiante(this.formEdit.value).subscribe({
        next: (response: Response) => {
          this.get(this.estudianteId);
          this.elementEditShow = false;
          this.elementEditLoading = false;
          this.loader.stop();
        },
        error: (err:HttpErrorResponse) => {
          this.elementEditLoading = false;
          this.loader.stop();
        }
      })
    }
  }

  setEdit(element:any){
    this.formEdit.controls['asistencia_justificacion'].setValue(element.asistencia_justificacion);
    this.formEdit.controls['id'].setValue(element.id);
    this.elementEditShow = true;
  }
}
