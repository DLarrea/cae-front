import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { CursosService } from 'src/app/services/cursos.service';
import { Response } from 'src/app/utils/response';
import { itemHome } from 'src/globals';

@Component({
  selector: 'app-materias-list',
  templateUrl: './materias-list.component.html',
  styleUrls: ['./materias-list.component.scss']
})
export class MateriasListComponent {
  materias: any[] =[];
  curso: any=null;
  currentUser: any = null;
  items: MenuItem[] = [];
  home: MenuItem = itemHome;

  constructor(private cursoService: CursosService, private route:ActivatedRoute, private loader: NgxUiLoaderService, private authService: AuthService){
    this.authService.getUser().subscribe(
      data => {
        if(data != null){
          this.currentUser = data;
          const cursoId = this.route.snapshot.paramMap.get('curso');
          this.get(this.currentUser.id, Number(cursoId));
        }
      }
    )
  }


  ngOnInit(): void {
    
  }
  

  get = (id:number, cursoId: number) => {
    this.loader.start();
    this.cursoService.getProfesorCursosMateriasById(id,cursoId).subscribe({
      next: (response: Response) => {
        this.materias = response.result.materias;
        this.curso = response.result.curso;
        this.items = [{ label : this.curso.nombre }]
        this.loader.stop();
      },
      error: (err:HttpErrorResponse) => {
        this.materias = [];
        this.loader.stop();
      }
    })
  }
  
}
