import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-card-home',
  templateUrl: './card-home.component.html',
  styleUrls: ['./card-home.component.scss']
})
export class CardHomeComponent {

  @Input() title!: string;
  @Input() icon!: string;
  @Input() button!: string;
  @Input() route!: string[];
  constructor(private router: Router) {}

  changeRoute() {
    this.router.navigate(this.route);
  }
} 
