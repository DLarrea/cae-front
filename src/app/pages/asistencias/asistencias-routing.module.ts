import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AsistenciasComponent } from './asistencias.component';
import { MateriasListComponent } from './materias-list/materias-list.component';
import { AsistenciasListComponent } from './asistencias-list/asistencias-list.component';
import { HistorialListComponent } from './historial-list/historial-list.component';
import { AsistenciasEstudianteComponent } from './asistencias-estudiante/asistencias-estudiante.component';

const routes: Routes = [
  {
    path: '',
    component: AsistenciasComponent,
    children: [
      {
        path: ':curso',
        component: MateriasListComponent
      },
      {
        path: 'estudiante/:id',
        component: AsistenciasEstudianteComponent
      },
      {
        path: ':curso/asistencia/:materia',
        component: AsistenciasListComponent 
      },
      {
        path: ':curso/asistencia/:materia/historial',
        component: HistorialListComponent 
      }
    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsistenciasRoutingModule { }
