import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsistenciasRoutingModule } from './asistencias-routing.module';
import { MateriasListComponent } from './materias-list/materias-list.component';
import { AsistenciasComponent } from './asistencias.component';
import { PagesPrimengModule } from 'src/app/primeng/pages-primeng.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AsistenciasListComponent } from './asistencias-list/asistencias-list.component';
import { CardHomeComponent } from './shared/card-home/card-home.component';
import { HistorialListComponent } from './historial-list/historial-list.component';
import { AsistenciasEstudianteComponent } from './asistencias-estudiante/asistencias-estudiante.component';


@NgModule({
  declarations: [
    AsistenciasComponent,
    MateriasListComponent,
    AsistenciasListComponent,
    CardHomeComponent,
    HistorialListComponent,
    AsistenciasEstudianteComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    PagesPrimengModule,
    AsistenciasRoutingModule
  ]
})
export class AsistenciasModule { }
