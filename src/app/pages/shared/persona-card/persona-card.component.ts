import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-persona-card',
  templateUrl: './persona-card.component.html',
  styleUrls: ['./persona-card.component.scss']
})
export class PersonaCardComponent {
  @Input() title!: string;
  @Input() description!: string;
  @Input() icon!: string;
}
