import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgxPermissionsService } from 'ngx-permissions';
import { MenuItem } from 'primeng/api';
import { AuthService } from 'src/app/services/auth.service';
import { environment } from 'src/environments/environment.development';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {
  env = environment;
  showSidebar: boolean = false;
  sidebarItems: MenuItem[] = [
    {
      icon: 'pi pi-home',
      label: 'Inicio',
      command: () => {
        this.router.navigate(['/inicio']);
        this.showSidebar = false;
      }
    },
  ]
  currentUser: any = null;
  showCerrarSesion: boolean = false;

  constructor(private router: Router, private authService: AuthService, private permissionService: NgxPermissionsService){
    this.authService.getUser().subscribe(
      data => {
        if(data != null){
          this.currentUser = data;
          if('rol' in this.currentUser){
            this.permissionService.addPermission(this.currentUser.rol);
            if(this.currentUser.rol === 'ADMIN'){
              this.sidebarItems = [... this.sidebarItems, {
                icon: 'pi pi-wrench',
                label: 'Administración',
                items: [
                  {
                    icon: 'pi pi-flag-fill',
                    label: 'Materias',
                    command: () => {
                      this.router.navigate(['/admin/materias']);
                      this.showSidebar = false;
                    }
                  },
                  {
                    icon: 'pi pi-th-large',
                    label: 'Cursos',
                    command: () => {
                      this.router.navigate(['/admin/cursos']);
                      this.showSidebar = false;
                    }
                  },
                  {
                    icon: 'pi pi-user',
                    label: 'Estudiantes',
                    command: () => {
                      this.router.navigate(['/admin/estudiantes']);
                      this.showSidebar = false;
                    }
                  },
                  {
                    icon: 'pi pi-users',
                    label: 'Responsables',
                    command: () => {
                      this.router.navigate(['/admin/responsables']);
                      this.showSidebar = false;
                    }
                  },
                  {
                    icon: 'pi pi-user-edit',
                    label: 'Profesores',
                    command: () => {
                      this.router.navigate(['/admin/profesores']);
                      this.showSidebar = false;
                    }
                  },
                  {
                    icon: 'pi pi-cog',
                    label: 'Usuarios',
                    command: () => {
                      this.router.navigate(['/admin/usuarios']);
                      this.showSidebar = false;
                    }
                  },
                ]
              }]
            }
          }
        }
      }
    )  
  }

  logout = () => {
    this.authService.clearUser();
    localStorage.removeItem('token');
    this.router.navigate(['/iniciar-sesion']);
  }
}
